package com.zhiche.wms.app.controller.opbaas;


import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.dto.opbaas.paramdto.ExceptionRegisterParamDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ExConfigurationDTO;
import com.zhiche.wms.service.opbaas.IExceptionConfigurationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 异常配置 前端控制器
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Api(value = "exceptionConfiguration-API", description = "异常配置项接口")
@RestController
@RequestMapping("/exceptionConfiguration")
public class ExceptionConfigurationController {

    @Autowired
    private IExceptionConfigurationService iExceptionConfigurationService;

    @ApiOperation(value = "获取异常菜单", notes = "获取异常菜单操作")
    @PostMapping("/configurationList")
    public ResultDTO<List<ExConfigurationDTO>> getExceptionConfiguration(ExceptionRegisterParamDTO dto) {
        List<ExConfigurationDTO> dtoList = iExceptionConfigurationService.getConfigurationList(dto);
        return new ResultDTO<>(true, dtoList, "查询成功!");
    }


}

