package com.zhiche.wms.dto.inbound;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 入库通知单及明细数据传输对象
 * </p>
 */
public class InboundNoticeParamDTO implements Serializable {

    private String detailLines;
    private List<InboundNoticeLineParamDTO> lines;
    private String userCreate;
    private String userModified;

    /**
     * 收货仓库
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 货主
     */
    private String ownerId;
    /**
     * 货主单号
     */
    private String ownerOrderNo;
    /**
     * 下单日期
     */
    private Date orderDate;
    /**
     * 供应商
     */
    private String carrierId;
    /**
     * 供应商名称
     */
    private String carrierName;
    /**
     * 运输方式
     */
    private String transportMethod;
    /**
     * 车船号
     */
    private String plateNumber;
    /**
     * 司机姓名
     */
    private String driverName;
    /**
     * 司机联系方式
     */
    private String driverPhone;
    /**
     * 预计到货日期
     */
    private Date expectRecvDateLower;
    /**
     * 预计收货时间上限
     */
    private Date expectRecvDateUpper;
    /**
     * 来源唯一键
     */
    private String sourceKey;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 预计入库数量
     */
    private BigDecimal expectSumQty;
    /**
     * 来源单据号
     */
    private String sourceNo;
    /**
     * 明细行数
     */
    private Integer lineCount;
    /**
     * 备注
     */
    private String remarks;

    public List<InboundNoticeLineParamDTO> getLines() {
        return lines;
    }

    public void setLines(List<InboundNoticeLineParamDTO> lines) {
        this.lines = lines;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getDetailLines() {
        return detailLines;
    }

    public void setDetailLines(String detailLines) {
        this.detailLines = detailLines;
    }

    public String getSourceKey() {
        return sourceKey;
    }

    public void setSourceKey(String sourceKey) {
        this.sourceKey = sourceKey;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerOrderNo() {
        return ownerOrderNo;
    }

    public void setOwnerOrderNo(String ownerOrderNo) {
        this.ownerOrderNo = ownerOrderNo;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getCarrierId() {
        return carrierId;
    }

    public void setCarrierId(String carrierId) {
        this.carrierId = carrierId;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getTransportMethod() {
        return transportMethod;
    }

    public void setTransportMethod(String transportMethod) {
        this.transportMethod = transportMethod;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public Date getExpectRecvDateLower() {
        return expectRecvDateLower;
    }

    public void setExpectRecvDateLower(Date expectRecvDateLower) {
        this.expectRecvDateLower = expectRecvDateLower;
    }

    public Date getExpectRecvDateUpper() {
        return expectRecvDateUpper;
    }

    public void setExpectRecvDateUpper(Date expectRecvDateUpper) {
        this.expectRecvDateUpper = expectRecvDateUpper;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getExpectSumQty() {
        return expectSumQty;
    }

    public void setExpectSumQty(BigDecimal expectSumQty) {
        this.expectSumQty = expectSumQty;
    }

    public String getSourceNo() {
        return sourceNo;
    }

    public void setSourceNo(String sourceNo) {
        this.sourceNo = sourceNo;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
