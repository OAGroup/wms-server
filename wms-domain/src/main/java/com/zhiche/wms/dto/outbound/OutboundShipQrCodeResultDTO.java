package com.zhiche.wms.dto.outbound;

import java.io.Serializable;
import java.util.Date;

public class OutboundShipQrCodeResultDTO implements Serializable {

    private String noticeLineId;
    private String lotNo0;
    private String lotNo1;
    private String lotNo4;
    private String noticeLineStatus;
    private Date noticeLineCreate;
    private String lineSourceNo;
    private String lineSourceKey;
    private String noticeHeadId;
    private String noticeHeadStatus;
    private String noticeHeadSourceNo;
    private String noticeHeadSourceKey;
    private String carrierName;
    private String plateNumber;
    private String driverName;
    private String driverPhone;
    private String storeHouseId;
    private String ownerId;
    private String materielCode;
    private String materielName;


    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getMaterielCode() {
        return materielCode;
    }

    public void setMaterielCode(String materielCode) {
        this.materielCode = materielCode;
    }

    public String getMaterielName() {
        return materielName;
    }

    public void setMaterielName(String materielName) {
        this.materielName = materielName;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getNoticeLineStatus() {
        return noticeLineStatus;
    }

    public void setNoticeLineStatus(String noticeLineStatus) {
        this.noticeLineStatus = noticeLineStatus;
    }

    public Date getNoticeLineCreate() {
        return noticeLineCreate;
    }

    public void setNoticeLineCreate(Date noticeLineCreate) {
        this.noticeLineCreate = noticeLineCreate;
    }

    public String getLineSourceNo() {
        return lineSourceNo;
    }

    public void setLineSourceNo(String lineSourceNo) {
        this.lineSourceNo = lineSourceNo;
    }

    public String getLineSourceKey() {
        return lineSourceKey;
    }

    public void setLineSourceKey(String lineSourceKey) {
        this.lineSourceKey = lineSourceKey;
    }

    public String getNoticeHeadStatus() {
        return noticeHeadStatus;
    }

    public void setNoticeHeadStatus(String noticeHeadStatus) {
        this.noticeHeadStatus = noticeHeadStatus;
    }

    public String getNoticeHeadSourceNo() {
        return noticeHeadSourceNo;
    }

    public void setNoticeHeadSourceNo(String noticeHeadSourceNo) {
        this.noticeHeadSourceNo = noticeHeadSourceNo;
    }

    public String getNoticeHeadSourceKey() {
        return noticeHeadSourceKey;
    }

    public void setNoticeHeadSourceKey(String noticeHeadSourceKey) {
        this.noticeHeadSourceKey = noticeHeadSourceKey;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getNoticeLineId() {
        return noticeLineId;
    }

    public void setNoticeLineId(String noticeLineId) {
        this.noticeLineId = noticeLineId;
    }

    public String getNoticeHeadId() {
        return noticeHeadId;
    }

    public void setNoticeHeadId(String noticeHeadId) {
        this.noticeHeadId = noticeHeadId;
    }

    public String getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(String storeHouseId) {
        this.storeHouseId = storeHouseId;
    }
}
