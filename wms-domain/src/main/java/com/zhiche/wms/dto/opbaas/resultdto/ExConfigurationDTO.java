package com.zhiche.wms.dto.opbaas.resultdto;

import java.io.Serializable;
import java.util.List;

public class ExConfigurationDTO implements Serializable {
    private String id;
    private String level1Code;
    private String level1Name;
    private String level1Sort;
    private String level2Code;
    private String level2Name;
    private String level2Sort;
    private String vin;
    private String orderNo;
    private List<ExceptionRegisterDetailDTO> exceptionDetail;

    public String getLevel2Code() {
        return level2Code;
    }

    public void setLevel2Code(String level2Code) {
        this.level2Code = level2Code;
    }

    public String getLevel2Name() {
        return level2Name;
    }

    public void setLevel2Name(String level2Name) {
        this.level2Name = level2Name;
    }

    public String getLevel2Sort() {
        return level2Sort;
    }

    public void setLevel2Sort(String level2Sort) {
        this.level2Sort = level2Sort;
    }

    public String getLevel1Sort() {
        return level1Sort;
    }

    public void setLevel1Sort(String level1Sort) {
        this.level1Sort = level1Sort;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public List<ExceptionRegisterDetailDTO> getExceptionDetail() {
        return exceptionDetail;
    }

    public void setExceptionDetail(List<ExceptionRegisterDetailDTO> exceptionDetail) {
        this.exceptionDetail = exceptionDetail;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel1Code() {
        return level1Code;
    }

    public void setLevel1Code(String level1Code) {
        this.level1Code = level1Code;
    }

    public String getLevel1Name() {
        return level1Name;
    }

    public void setLevel1Name(String level1Name) {
        this.level1Name = level1Name;
    }
}
