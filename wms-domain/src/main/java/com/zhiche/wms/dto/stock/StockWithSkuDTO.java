package com.zhiche.wms.dto.stock;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class StockWithSkuDTO implements Serializable {
    @JsonSerialize(using=ToStringSerializer.class)
    private Long stockId;

    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;

    @JsonSerialize(using=ToStringSerializer.class)
    private Long locationId;

    @JsonSerialize(using=ToStringSerializer.class)
    private Long skuId;

    private BigDecimal qty;

    private BigDecimal netWeight;

    private BigDecimal grossWeight;

    private BigDecimal grossCubage;

    private BigDecimal packedCount;
    /**
     * 状态(10:正常,20:失效)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 货主
     */
    private String ownerId;
    /**
     * 物料ID
     */
    private String materielId;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 批号0
     */
    private String lotNo0;
    /**
     * 批号1
     */
    private String lotNo1;
    /**
     * 批号2
     */
    private String lotNo2;
    /**
     * 批号3
     */
    private String lotNo3;
    /**
     * 批号4
     */
    private String lotNo4;
    /**
     * 批号5
     */
    private String lotNo5;
    /**
     * 批号6
     */
    private String lotNo6;
    /**
     * 批号7
     */
    private String lotNo7;
    /**
     * 批号8
     */
    private String lotNo8;
    /**
     * 批号9
     */
    private String lotNo9;


    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getGrossCubage() {
        return grossCubage;
    }

    public void setGrossCubage(BigDecimal grossCubage) {
        this.grossCubage = grossCubage;
    }

    public BigDecimal getPackedCount() {
        return packedCount;
    }

    public void setPackedCount(BigDecimal packedCount) {
        this.packedCount = packedCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getLotNo0() {
        return lotNo0;
    }

    public void setLotNo0(String lotNo0) {
        this.lotNo0 = lotNo0;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getLotNo2() {
        return lotNo2;
    }

    public void setLotNo2(String lotNo2) {
        this.lotNo2 = lotNo2;
    }

    public String getLotNo3() {
        return lotNo3;
    }

    public void setLotNo3(String lotNo3) {
        this.lotNo3 = lotNo3;
    }

    public String getLotNo4() {
        return lotNo4;
    }

    public void setLotNo4(String lotNo4) {
        this.lotNo4 = lotNo4;
    }

    public String getLotNo5() {
        return lotNo5;
    }

    public void setLotNo5(String lotNo5) {
        this.lotNo5 = lotNo5;
    }

    public String getLotNo6() {
        return lotNo6;
    }

    public void setLotNo6(String lotNo6) {
        this.lotNo6 = lotNo6;
    }

    public String getLotNo7() {
        return lotNo7;
    }

    public void setLotNo7(String lotNo7) {
        this.lotNo7 = lotNo7;
    }

    public String getLotNo8() {
        return lotNo8;
    }

    public void setLotNo8(String lotNo8) {
        this.lotNo8 = lotNo8;
    }

    public String getLotNo9() {
        return lotNo9;
    }

    public void setLotNo9(String lotNo9) {
        this.lotNo9 = lotNo9;
    }
}
