package com.zhiche.wms.dto.outbound;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 12:23 2019/1/20
 */
public class PreparationPrintDTO {

    private String keyprepareId;
    private String shipmentGid;
    private String vehicleDescribe;
    private String vin;
    private String destLocationAddress;
    private String destLocationCity;
    private String destLocationCounty;
    private String destLocationGid;
    private String destLocationName;
    private String destLocationProvince;
    private String storeHouseCode;
    private String areaCode;
    private String locationCode;

    public String getKeyprepareId () {
        return keyprepareId;
    }

    public void setKeyprepareId (String keyprepareId) {
        this.keyprepareId = keyprepareId;
    }

    public String getShipmentGid () {
        return shipmentGid;
    }

    public void setShipmentGid (String shipmentGid) {
        this.shipmentGid = shipmentGid;
    }

    public String getVehicleDescribe () {
        return vehicleDescribe;
    }

    public void setVehicleDescribe (String vehicleDescribe) {
        this.vehicleDescribe = vehicleDescribe;
    }

    public String getVin () {
        return vin;
    }

    public void setVin (String vin) {
        this.vin = vin;
    }

    public String getDestLocationAddress () {
        return destLocationAddress;
    }

    public void setDestLocationAddress (String destLocationAddress) {
        this.destLocationAddress = destLocationAddress;
    }

    public String getDestLocationCity () {
        return destLocationCity;
    }

    public void setDestLocationCity (String destLocationCity) {
        this.destLocationCity = destLocationCity;
    }

    public String getDestLocationCounty () {
        return destLocationCounty;
    }

    public void setDestLocationCounty (String destLocationCounty) {
        this.destLocationCounty = destLocationCounty;
    }

    public String getDestLocationGid () {
        return destLocationGid;
    }

    public void setDestLocationGid (String destLocationGid) {
        this.destLocationGid = destLocationGid;
    }

    public String getDestLocationName () {
        return destLocationName;
    }

    public void setDestLocationName (String destLocationName) {
        this.destLocationName = destLocationName;
    }

    public String getDestLocationProvince () {
        return destLocationProvince;
    }

    public void setDestLocationProvince (String destLocationProvince) {
        this.destLocationProvince = destLocationProvince;
    }

    public String getStoreHouseCode () {
        return storeHouseCode;
    }

    public void setStoreHouseCode (String storeHouseCode) {
        this.storeHouseCode = storeHouseCode;
    }

    public String getAreaCode () {
        return areaCode;
    }

    public void setAreaCode (String areaCode) {
        this.areaCode = areaCode;
    }

    public String getLocationCode () {
        return locationCode;
    }

    public void setLocationCode (String locationCode) {
        this.locationCode = locationCode;
    }

    @Override
    public String toString () {
        final StringBuffer sb = new StringBuffer("PreparationPrintDTO{");
        sb.append("keyprepareId='").append(keyprepareId).append('\'');
        sb.append(", shipmentGid='").append(shipmentGid).append('\'');
        sb.append(", vehicleDescribe='").append(vehicleDescribe).append('\'');
        sb.append(", vin='").append(vin).append('\'');
        sb.append(", destLocationAddress='").append(destLocationAddress).append('\'');
        sb.append(", destLocationCity='").append(destLocationCity).append('\'');
        sb.append(", destLocationCounty='").append(destLocationCounty).append('\'');
        sb.append(", destLocationGid='").append(destLocationGid).append('\'');
        sb.append(", destLocationName='").append(destLocationName).append('\'');
        sb.append(", destLocationProvince='").append(destLocationProvince).append('\'');
        sb.append(", storeHouseCode='").append(storeHouseCode).append('\'');
        sb.append(", areaCode='").append(areaCode).append('\'');
        sb.append(", locationCode='").append(locationCode).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

