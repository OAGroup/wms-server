package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;

public class MissingRegisterParamDTO implements Serializable {

    /**
     * 仓库ID
     */
    private String houseId;
    /**
     * 节点ID
     */
    private String taskId;
    /**
     * 车架号
     */
    private String vin;
    /**
     * 订单号
     */
    private String orderNo;
    /**
     * 作业节点
     */
    private String taskNode;
    /**
     * 缺件标识
     */
    private String codes;
    /**
     * 缺件名称
     */
    private String names;
    /**
     * 缺件描述
     */
    private String describe;

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTaskNode() {
        return taskNode;
    }

    public void setTaskNode(String taskNode) {
        this.taskNode = taskNode;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }
}
