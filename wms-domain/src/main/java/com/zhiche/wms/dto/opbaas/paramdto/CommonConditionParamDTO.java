package com.zhiche.wms.dto.opbaas.paramdto;

import java.io.Serializable;
import java.util.Map;

public class CommonConditionParamDTO implements Serializable {

    private Map<String, String> condition;


    public Map<String, String> getCondition() {
        return condition;
    }

    public void setCondition(Map<String, String> condition) {
        this.condition = condition;
    }
}
