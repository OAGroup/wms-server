package com.zhiche.wms.dto.inbound;

import java.io.Serializable;

public class LocChangeDTO implements Serializable {

    private String ownerId;
    private String materielId;
    private String sourceLoc;
    private String destLoc;
    private String lotNo1;
    private String remarks;
    private String gmtCreate;
    private String userCreate;
    private String storeHouseId;


    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getMaterielId() {
        return materielId;
    }

    public void setMaterielId(String materielId) {
        this.materielId = materielId;
    }

    public String getSourceLoc() {
        return sourceLoc;
    }

    public void setSourceLoc(String sourceLoc) {
        this.sourceLoc = sourceLoc;
    }

    public String getDestLoc() {
        return destLoc;
    }

    public void setDestLoc(String destLoc) {
        this.destLoc = destLoc;
    }

    public String getLotNo1() {
        return lotNo1;
    }

    public void setLotNo1(String lotNo1) {
        this.lotNo1 = lotNo1;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(String storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    @Override
    public String toString() {
        return "LocChangeDTO{" +
                "ownerId='" + ownerId + '\'' +
                ", materielId='" + materielId + '\'' +
                ", sourceLoc='" + sourceLoc + '\'' +
                ", destLoc='" + destLoc + '\'' +
                ", lotNo1='" + lotNo1 + '\'' +
                ", remarks='" + remarks + '\'' +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", userCreate='" + userCreate + '\'' +
                ", storeHouseId='" + storeHouseId + '\'' +
                '}';
    }
}
