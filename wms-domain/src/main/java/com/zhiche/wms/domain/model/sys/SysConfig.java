package com.zhiche.wms.domain.model.sys;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * WMS各功能节点配置
 * </p>
 *
 * @author yangzhiyu
 * @since 2018-11-30
 */
@TableName("w_sys_config")
public class SysConfig extends Model<SysConfig> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 配置编码
     */
	private String code;
    /**
     * 配置名称
     */
	private String label;
    /**
     * 配置项说明
     */
	private String comment;
    /**
     * 关联类型的唯一标识
     */
	@TableField("releation_id")
	private String releationId;
    /**
     * 配置类型ROLE:基于角色,POINT:基于发车点,WAREHOUSE 基于仓库配置...
     */
	@TableField("config_type")
	private String configType;
    /**
     * 配置级别 0必须不 1无影响 2必须
     */
	@TableField("config_level")
	private String configLevel;
    /**
     * = 等于,> 大于,<小于,≠ 不等于,≥ 大于等于,≤小于等于
     */
	@TableField("config_condition")
	private String configCondition;
    /**
     * 配置数据
     */
	@TableField("config_detail")
	private String configDetail;
    /**
     * 系统创建时间
     */
	@TableField("gmt_create")
	private Date gmtCreate;
    /**
     * 更新时间
     */
	@TableField("gmt_update")
	private Date gmtUpdate;
    /**
     * 创建人-名字
     */
	@TableField("create_user")
	private String createUser;
    /**
     * 配置状态--0可用 1已删除 2禁用
     */
	@TableField("config_status")
	private String configStatus;

	/**
	 * 是否已配置
	 */
	@TableField("configed")
	private String configed;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getReleationId() {
		return releationId;
	}

	public void setReleationId(String releationId) {
		this.releationId = releationId;
	}

	public String getConfigType() {
		return configType;
	}

	public void setConfigType(String configType) {
		this.configType = configType;
	}

	public String getConfigLevel() {
		return configLevel;
	}

	public void setConfigLevel(String configLevel) {
		this.configLevel = configLevel;
	}

	public String getConfigCondition() {
		return configCondition;
	}

	public void setConfigCondition(String configCondition) {
		this.configCondition = configCondition;
	}

	public String getConfigDetail() {
		return configDetail;
	}

	public void setConfigDetail(String configDetail) {
		this.configDetail = configDetail;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtUpdate() {
		return gmtUpdate;
	}

	public void setGmtUpdate(Date gmtUpdate) {
		this.gmtUpdate = gmtUpdate;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getConfigStatus() {
		return configStatus;
	}

	public void setConfigStatus(String configStatus) {
		this.configStatus = configStatus;
	}

	public String getConfiged() {
		return configed;
	}

	public void setConfiged(String configed) {
		this.configed = configed;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "SysConfig{" +
				"id=" + id +
				", code='" + code + '\'' +
				", label='" + label + '\'' +
				", comment='" + comment + '\'' +
				", releationId='" + releationId + '\'' +
				", configType='" + configType + '\'' +
				", configLevel='" + configLevel + '\'' +
				", configCondition='" + configCondition + '\'' +
				", configDetail='" + configDetail + '\'' +
				", gmtCreate=" + gmtCreate +
				", gmtUpdate=" + gmtUpdate +
				", createUser='" + createUser + '\'' +
				", configStatus='" + configStatus + '\'' +
				", configed='" + configed + '\'' +
				'}';
	}
}
