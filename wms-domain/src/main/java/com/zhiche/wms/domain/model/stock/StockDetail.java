package com.zhiche.wms.domain.model.stock;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 库存明细账
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@TableName("w_stock_detail")
public class StockDetail extends Model<StockDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 库存ID
     */
    @TableField("stock_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long stockId;
    /**
     * 业务类型(10::移位,20:期初库存,30:库存调整)
     */
    @TableField("business_type")
    private String businessType;
    /**
     * 相关明细行键
     */
    @TableField("relation_line_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long relationLineId;
    /**
     * 类型(10:增加,20:减少)
     */
    private String type;
    /**
     * 数量
     */
    private BigDecimal qty;
    /**
     * 净重
     */
    @TableField("net_weight")
    private BigDecimal netWeight;
    /**
     * 毛重
     */
    @TableField("gross_weight")
    private BigDecimal grossWeight;
    /**
     * 体积
     */
    @TableField("gross_cubage")
    private BigDecimal grossCubage;
    /**
     * 件数
     */
    @TableField("packed_count")
    private BigDecimal packedCount;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Long getRelationLineId() {
        return relationLineId;
    }

    public void setRelationLineId(Long relationLineId) {
        this.relationLineId = relationLineId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getQty() {
        return qty;
    }

    public void setQty(BigDecimal qty) {
        this.qty = qty;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public BigDecimal getGrossCubage() {
        return grossCubage;
    }

    public void setGrossCubage(BigDecimal grossCubage) {
        this.grossCubage = grossCubage;
    }

    public BigDecimal getPackedCount() {
        return packedCount;
    }

    public void setPackedCount(BigDecimal packedCount) {
        this.packedCount = packedCount;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "StockDetail{" +
                ", id=" + id +
                ", stockId=" + stockId +
                ", businessType=" + businessType +
                ", relationLineId=" + relationLineId +
                ", type=" + type +
                ", qty=" + qty +
                ", netWeight=" + netWeight +
                ", grossWeight=" + grossWeight +
                ", grossCubage=" + grossCubage +
                ", packedCount=" + packedCount +
                ", gmtCreate=" + gmtCreate +
                "}";
    }
}
