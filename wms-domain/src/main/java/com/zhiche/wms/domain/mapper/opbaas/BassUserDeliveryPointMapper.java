package com.zhiche.wms.domain.mapper.opbaas;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.opbaas.OpUserDeliveryPoint;

/**
 * <p>
 * 用户发车点配置 Mapper 接口
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
public interface BassUserDeliveryPointMapper extends BaseMapper<OpUserDeliveryPoint> {

}
