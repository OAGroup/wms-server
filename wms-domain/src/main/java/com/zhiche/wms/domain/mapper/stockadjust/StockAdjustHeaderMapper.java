package com.zhiche.wms.domain.mapper.stockadjust;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustHeader;

/**
 * <p>
 * 库存调整单头 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface StockAdjustHeaderMapper extends BaseMapper<StockAdjustHeader> {

}
