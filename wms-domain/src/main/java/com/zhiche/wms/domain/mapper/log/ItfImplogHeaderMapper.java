package com.zhiche.wms.domain.mapper.log;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.log.ItfImplogHeader;

/**
 * <p>
 * 仓储接口导入日志头 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-11
 */
public interface ItfImplogHeaderMapper extends BaseMapper<ItfImplogHeader> {

}
