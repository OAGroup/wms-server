package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.outbound.OutboundKeyPrepareHeader;
import com.zhiche.wms.dto.outbound.PreparationListDetailDTO;

import java.util.HashMap;
import java.util.List;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:16 2018/12/19
 */
public interface OutboundKeyPrepareHeaderMapper extends BaseMapper<OutboundKeyPrepareHeader> {

    void updateSQLMode();

    List<PreparationListDetailDTO> selectKeyPrepareDetailListByParams (HashMap<String, Object> params);
}
