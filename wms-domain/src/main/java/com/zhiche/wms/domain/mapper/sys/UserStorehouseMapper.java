package com.zhiche.wms.domain.mapper.sys;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.sys.UserStorehouse;
import com.zhiche.wms.dto.inbound.StoreAreaAndLocationDTO;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户仓库权限表 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface UserStorehouseMapper extends BaseMapper<UserStorehouse> {

}
