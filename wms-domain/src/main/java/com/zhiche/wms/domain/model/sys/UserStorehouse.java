package com.zhiche.wms.domain.model.sys;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户仓库权限表
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
@TableName("sys_user_storehouse")
public class UserStorehouse extends Model<UserStorehouse> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 仓库ID
     */
    @JsonSerialize(using=ToStringSerializer.class)
    @TableField("store_house_id")
    private Long storeHouseId;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "UserStorehouse{" +
                ", id=" + id +
                ", userId=" + userId +
                ", storeHouseId=" + storeHouseId +
                ", gmtCreate=" + gmtCreate +
                "}";
    }
}
