package com.zhiche.wms.domain.mapper.outbound;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.domain.model.outbound.OutboundShipLine;
import com.zhiche.wms.dto.outbound.OutboundShipDTO;
import com.zhiche.wms.dto.outbound.OutboundShipQuitResultDTO;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 出库单明细 Mapper 接口
 * </p>
 *
 * @author qichao
 * @since 2018-06-08
 */
public interface OutboundShipLineMapper extends BaseMapper<OutboundShipLine> {

    OutboundShipDTO getPutWayByLineId(@Param("id") Long id);

    List<OutboundShipQuitResultDTO> selectQuitInfoByParams(HashMap<String, Object> params);

    List<OutboundShipDTO> queryLineListByPage(Page<OutboundShipDTO> page,
                                              @Param("ew") Wrapper<OutboundShipDTO> ew);

    List<OutboundShipDTO> selectExportOSData(@Param("ew") Wrapper<OutboundShipDTO> ew);
}
