package com.zhiche.wms.domain.mapper.stock;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.stock.SkuStore;

/**
 * <p>
 * 停放位置信息 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-10-19
 */
public interface SkuStoreMapper extends BaseMapper<SkuStore> {

}
