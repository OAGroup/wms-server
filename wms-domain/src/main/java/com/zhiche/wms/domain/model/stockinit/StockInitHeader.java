package com.zhiche.wms.domain.model.stockinit;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 期初库存头
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@TableName("w_stock_init_header")
public class StockInitHeader extends Model<StockInitHeader> {

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<StockInitLine> stockInitLineList;

    /**
     * id
     */
    @JsonSerialize(using=ToStringSerializer.class)
    private Long id;
    /**
     * 期初库存单号
     */
    @TableField("init_no")
    private String initNo;
    /**
     * 仓库
     */
    @TableField("store_house_id")
    @JsonSerialize(using=ToStringSerializer.class)
    private Long storeHouseId;
    /**
     * 货主
     */
    @TableField("owner_id")
    private String ownerId;
    /**
     * 单据日期
     */
    @TableField("order_date")
    private Date orderDate;
    /**
     * 计量单位
     */
    private String uom;
    /**
     * 数量
     */
    @TableField("sum_qty")
    private BigDecimal sumQty;
    /**
     * 明细行数
     */
    @TableField("line_count")
    private Integer lineCount;
    /**
     * 状态(10:已保存,20:已审核,30:已撤销)
     */
    private String status;
    /**
     * 备注
     */
    private String remarks;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInitNo() {
        return initNo;
    }

    public void setInitNo(String initNo) {
        this.initNo = initNo;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public void setStoreHouseId(Long storeHouseId) {
        this.storeHouseId = storeHouseId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getSumQty() {
        return sumQty;
    }

    public void setSumQty(BigDecimal sumQty) {
        this.sumQty = sumQty;
    }

    public Integer getLineCount() {
        return lineCount;
    }

    public void setLineCount(Integer lineCount) {
        this.lineCount = lineCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "StockInitHeader{" +
                ", id=" + id +
                ", initNo=" + initNo +
                ", storeHouseId=" + storeHouseId +
                ", ownerId=" + ownerId +
                ", orderDate=" + orderDate +
                ", uom=" + uom +
                ", sumQty=" + sumQty +
                ", lineCount=" + lineCount +
                ", status=" + status +
                ", remarks=" + remarks +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }

    public List<StockInitLine> getStockInitLineList() {
        return stockInitLineList;
    }

    public void setStockInitLineList(List<StockInitLine> stockInitLineList) {
        this.stockInitLineList = stockInitLineList;
    }

    public void addStockInitLine(StockInitLine stockInitLine) {
        if (Objects.isNull(stockInitLineList)) {
            stockInitLineList = new ArrayList<>();
        }
        stockInitLineList.add(stockInitLine);
    }
}
