package com.zhiche.wms.domain.mapper.sys;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.zhiche.wms.domain.model.sys.UserDeliveryPoint;

/**
 * <p>
 * 用户发车点权限表 Mapper 接口
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-23
 */
public interface UserDeliveryPointMapper extends BaseMapper<UserDeliveryPoint> {

}
