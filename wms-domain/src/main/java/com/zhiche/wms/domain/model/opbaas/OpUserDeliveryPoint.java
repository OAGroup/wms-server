package com.zhiche.wms.domain.model.opbaas;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户发车点配置
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@TableName("op_user_delivery_point")
public class OpUserDeliveryPoint extends Model<OpUserDeliveryPoint> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;
    /**
     * 用户编码
     */
    @TableField("user_code")
    private String userCode;
    /**
     * 发车点编码
     */
    @TableField("point_code")
    private String pointCode;
    /**
     * 需寻车
     */
    @TableField("is_seek")
    private String isSeek;
    /**
     * 需移车
     */
    @TableField("is_move")
    private String isMove;
    /**
     * 需提车
     */
    @TableField("is_pick")
    private String isPick;
    /**
     * 状态(10：正常，20：失效)
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建人
     */
    @TableField("user_create")
    private String userCreate;
    /**
     * 修改人
     */
    @TableField("user_modified")
    private String userModified;
    /**
     * 创建时间
     */
    @TableField("gmt_create")
    private Date gmtCreate;
    /**
     * 修改时间
     */
    @TableField("gmt_modified")
    private Date gmtModified;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getPointCode() {
        return pointCode;
    }

    public void setPointCode(String pointCode) {
        this.pointCode = pointCode;
    }

    public String getIsSeek() {
        return isSeek;
    }

    public void setIsSeek(String isSeek) {
        this.isSeek = isSeek;
    }

    public String getIsMove() {
        return isMove;
    }

    public void setIsMove(String isMove) {
        this.isMove = isMove;
    }

    public String getIsPick() {
        return isPick;
    }

    public void setIsPick(String isPick) {
        this.isPick = isPick;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserModified() {
        return userModified;
    }

    public void setUserModified(String userModified) {
        this.userModified = userModified;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "OpUserDeliveryPoint{" +
                ", id=" + id +
                ", userCode=" + userCode +
                ", pointCode=" + pointCode +
                ", isSeek=" + isSeek +
                ", isMove=" + isMove +
                ", isPick=" + isPick +
                ", status=" + status +
                ", remark=" + remark +
                ", userCreate=" + userCreate +
                ", userModified=" + userModified +
                ", gmtCreate=" + gmtCreate +
                ", gmtModified=" + gmtModified +
                "}";
    }
}
