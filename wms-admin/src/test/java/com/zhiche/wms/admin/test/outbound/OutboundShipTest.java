package com.zhiche.wms.admin.test.outbound;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import com.zhiche.wms.WebApplication;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.InterfaceEventEnum;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.HttpClientUtil;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.model.log.ItfExplogLine;
import com.zhiche.wms.domain.model.opbaas.ExceptionRegister;
import com.zhiche.wms.domain.model.opbaas.StatusLog;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.domain.model.outbound.OutboundNoticeLine;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.service.common.IntegrationService;
import com.zhiche.wms.service.constant.PutAwayType;
import com.zhiche.wms.service.constant.ShipType;
import com.zhiche.wms.service.constant.SourceSystem;
import com.zhiche.wms.service.dto.OTMEvent;
import com.zhiche.wms.service.dto.ShipParamDTO;
import com.zhiche.wms.service.log.IItfExplogLineService;
import com.zhiche.wms.service.opbaas.IExceptionRegisterService;
import com.zhiche.wms.service.opbaas.IStatusLogService;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import com.zhiche.wms.service.outbound.IOutboundNoticeLineService;
import com.zhiche.wms.service.outbound.IOutboundPrepareHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipHeaderService;
import com.zhiche.wms.service.outbound.IOutboundShipLineService;
import com.zhiche.wms.service.utils.BusinessNodeExport;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by zhaoguixin on 2018/6/20.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OutboundShipTest {
    private static final Logger logger = LoggerFactory.getLogger(OutboundShipTest.class);

    @Autowired
    private IOutboundShipHeaderService outboundShipHeaderService;
    @Autowired
    private IOutboundShipLineService shipLineService;
    @Autowired
    private IOtmOrderReleaseService releaseService;
    @Autowired
    private IOtmShipmentService shipmentService;
    @Autowired
    private OtmOrderReleaseMapper otmOrderReleaseMapper;
    @Autowired
    private IItfExplogLineService logLineService;
    @Autowired
    private IStatusLogService statusLogService;
    @Autowired
    private IExceptionRegisterService exceptionRegisterService;
    @Autowired
    private IOutboundNoticeLineService outboundNoticeLineService;
    @Autowired
    private IOutboundShipLineService outboundShipLineService;
    @Autowired
    private IOutboundPrepareHeaderService prepareHeaderService;
    @Autowired
    private IntegrationService integrationService;

    @Test
    public void outboundByNoticeLineId() {
        shipLineService.shipByNoticeLineId(new Long("46716385618817024"),
                ShipType.NOTICE_SHIP, SourceSystem.HAND_MADE);
    }


    /**
     * <p>
     * 手动推送wms已发运数据到TMS(补推发运数据)
     * </p>
     */
    @Test
    public void outShipHandler() {
        EntityWrapper<OtmOrderRelease> oorEW = new EntityWrapper<>();
        oorEW.eq("status", TableStatusEnum.STATUS_BS_DISPATCH.getCode())
                //手动指定指令推送
                .in("shipment_gid", "S19012100559")
                .orderBy("id", false);
        int count = releaseService.selectCount(oorEW);
        Page<OtmOrderRelease> pg = new Page<>();
        pg.setSize(11000);
        pg.setCurrent(1);
        List<OtmOrderRelease> orderReleases = otmOrderReleaseMapper.selectPage(pg, oorEW);
        logger.info("wms共查询到-----{}条-----已发运车辆", count);
        int i = 0;
        for (OtmOrderRelease oor : orderReleases) {
            EntityWrapper<ItfExplogLine> logEW = new EntityWrapper<>();
            logEW.eq("relation_id", oor.getId())
                    .eq("export_type", InterfaceEventEnum.BS_OP_DELIVERY.getCode());
            List<ItfExplogLine> lines = logLineService.selectList(logEW);
            if (CollectionUtils.isNotEmpty(lines)) {
                ItfExplogLine explogLine = lines.get(0);
                // TMS正式地址: http://10.20.30.155:8280/lisa-tms/shipTask/shipDataForBMS
                try {
                    logger.info("wms推送TMS发运  releaseGid:{},shipmentGid:{} 次数i:{}", oor.getReleaseGid(), oor.getShipmentGid(), ++i);
                    String resJson = HttpClientUtil.postJson("http://10.20.30.155:8280/lisa-tms/shipTask/shipDataForBMS", null, explogLine.getDataContent(), 60000);
                    logger.info("wms推送TMS发运结果-->{}", resJson);
                } catch (Exception e) {
                    logger.error("wms推送TMS发运失败-->{}", e);
                }
            } else {
                logger.error("WMS推送发运失败--->or的表主键:{},运单:{},指令:{}", oor.getId(), oor.getReleaseGid(), oor.getShipmentGid());
            }
        }
    }

    /**
     * 自动已发运数据到TMS/BMS(用于手动调整WMS数据的)
     */
    @Test
    public void otmShipAuto() {
        EntityWrapper<ShipmentForShipDTO> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode())
                //指令号
                //.in("re_shipment_gid", "S19011900329,S19011900328,S19011900327,S19011900326,S19011900347,S19012100317,S19011900504,S19012100313,S19012100314,S19012100315,S19012100316")
                .ge("re_gmt_modify", "2019-01-22 13:50:00")
                .le("re_gmt_modify", "2019-01-22 13:57:00")
                .ne("releaseStatus", TableStatusEnum.STATUS_50.getCode())
                //.eq("is_ship", TableStatusEnum.STATUS_1.getCode())
                .orderBy("gmt_create", false)
                .orderBy("id", false)
                .orderBy("releaseId", false);
        List<ShipmentForShipDTO> list = shipmentService.getShipDetail(ew);
        if (CollectionUtils.isNotEmpty(list)) {
            for (ShipmentForShipDTO shipDTO : list) {
                OtmShipment os = new OtmShipment();
                os.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                os.setId(shipDTO.getId());
                shipmentService.updateById(os);
                new Thread(() -> {
                    StatusLog sl = new StatusLog();
                    sl.setTableType(TableStatusEnum.STATUS_20.getCode());
                    sl.setTableId(String.valueOf(shipDTO.getId()));
                    sl.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    sl.setStatusName(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    sl.setUserCreate("统一调整发运");
                    statusLogService.insert(sl);
                }).start();
                shipDTO.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                //更新运单明细 状态为已发运
                List<OtmOrderRelease> releaseList = shipDTO.getOtmOrderReleaseList();
                ArrayList<StatusLog> insertLogs = Lists.newArrayList();
                for (OtmOrderRelease oor : releaseList) {
                    Wrapper<ExceptionRegister> registerWrapper = new EntityWrapper<>();
                    registerWrapper.eq("vin", oor.getVin()).eq("otm_status", TableStatusEnum.STATUS_N.getCode());
                    ExceptionRegister exceptionRegister = exceptionRegisterService.selectOne(registerWrapper);
                    if (Objects.nonNull(exceptionRegister)) {
                        throw new BaseException("车架号:" + oor.getVin() + "异常不发运,请联系调度!");
                    }
                    OtmOrderRelease release = new OtmOrderRelease();
                    release.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    release.setId(oor.getId());
                    releaseService.updateById(release);
                    oor.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    StatusLog sl = new StatusLog();
                    sl.setTableType(TableStatusEnum.STATUS_10.getCode());
                    sl.setTableId(String.valueOf(release.getId()));
                    sl.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    sl.setStatusName(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    sl.setUserCreate("数据调整统一发运");
                    insertLogs.add(sl);
                    //发运  更新车辆为已出库
                    ArrayList<String> status = Lists.newArrayList();
                    status.add(TableStatusEnum.STATUS_10.getCode());
                    status.add(TableStatusEnum.STATUS_20.getCode());

                    EntityWrapper<OutboundNoticeLine> nlEW = new EntityWrapper<>();
                    nlEW.eq("line_source_key", oor.getReleaseGid())
                            .in("status", status)
                            .orderBy("id", false);
                    OutboundNoticeLine noticeLine = outboundNoticeLineService.selectOne(nlEW);
                    if (noticeLine != null) {
                        try {
                            //调用方法出库
                            outboundShipLineService.shipByNoticeLineId(noticeLine.getId(), PutAwayType.NOTICE_PUTAWAY, SourceSystem.AUTO);
                            //调用方法修改备料状态
                            prepareHeaderService.updatePrepareFinishByLineId(noticeLine.getId(), "统一数据调整发运");
                        } catch (Exception e) {
                            logger.error("自动调整出库失败 车架号:{}...运单号:{}", noticeLine.getLotNo1(), noticeLine.getLineSourceKey());
                        }
                    }
                    //new Thread(() -> {
                    OTMEvent otmEvent = integrationService.getOtmEvent(String.valueOf(release.getId()),
                            oor.getReleaseGid(),
                            InterfaceEventEnum.BS_OP_DELIVERY.getCode(),
                            oor.getShipmentGid(),
                            "已发运回传OTM/BMS");
                    //调整参数--传递更多信息
                    ShipParamDTO paramDTO = new ShipParamDTO();
                    paramDTO.setPlateNo(shipDTO.getPlateNo());
                    paramDTO.setTrailerNo(shipDTO.getTrailerNo());
                    paramDTO.setVehicleName(oor.getStanVehicleType());
                    paramDTO.setVin(oor.getVin());
                    paramDTO.setOriginProvince(oor.getOriginLocationProvince());
                    paramDTO.setOriginCity(oor.getOriginLocationCity());
                    paramDTO.setOriginCounty(oor.getOriginLocationCounty());
                    paramDTO.setOriginAddr(oor.getOriginLocationAddress());
                    paramDTO.setDestProvince(oor.getDestLocationProvince());
                    paramDTO.setDestCity(oor.getDestLocationCity());
                    paramDTO.setDestCounty(oor.getDestLocationCounty());
                    paramDTO.setDestAddr(oor.getDestLocationAddress());
                    paramDTO.setDriverGid(shipDTO.getDriverGid());
                    paramDTO.setProviderGid(shipDTO.getServiceProviderGid());
                    paramDTO.setShipTime(String.valueOf(new Date().getTime()));
                    paramDTO.setOriginCode(oor.getOriginLocationGid());
                    paramDTO.setDestCode(oor.getDestLocationGid());
                    paramDTO.setExportKey(otmEvent.getExportKey());
                    paramDTO.setCallBackUrl(otmEvent.getCallBackUrl());
                    paramDTO.setEventType(otmEvent.getEventType());
                    paramDTO.setOccurDate(otmEvent.getOccurDate());
                    paramDTO.setRecdDate(otmEvent.getRecdDate());
                    paramDTO.setSort(otmEvent.getSort());
                    paramDTO.setDescribe(otmEvent.getDescribe());
                    paramDTO.setOrderReleaseId(otmEvent.getOrderReleaseId());
                    paramDTO.setShipmentId(otmEvent.getShipmentId());
                    paramDTO.setQrCode(otmEvent.getQrCode());
                    paramDTO.setCusOrderNo(oor.getCusOrderNo());
                    paramDTO.setCusWaybill(oor.getCusWaybillNo());
                    //String res = nodeExport.exportEventToOTMNew(paramDTO);
                    try {
                        String paramJSON = JSONObject.toJSONString(paramDTO);
                        logger.info("wms推送TMS发运  releaseGid:{},shipmentGid:{}", oor.getReleaseGid(), oor.getShipmentGid());
                        String resJson = HttpClientUtil.postJson("http://10.20.30.155:8280/lisa-tms/shipTask/shipDataForBMS", null, paramJSON, 60000);
                        logger.info("wms推送TMS发运结果 -->{}", resJson);
                        integrationService.insertExportLogNew(String.valueOf(release.getId()),
                                paramDTO,
                                resJson,
                                "已发运回传OTM/BMS",
                                InterfaceEventEnum.BS_OP_DELIVERY.getCode());
                    } catch (Exception e) {
                        logger.error("wms推送TMS发运失败-->{}", e);
                    }

                    //}).start();
                }
                if (CollectionUtils.isNotEmpty(insertLogs)) {
                    statusLogService.insertBatch(insertLogs);
                }
            }
        }
    }
}
