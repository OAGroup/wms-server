package com.zhiche.wms.admin.vo.sys;

import com.zhiche.wms.domain.model.opbaas.OpDeliveryPoint;

import java.util.List;

/**
 * Created by hxh on 2018/9/20.
 */
public class UserDeliveryPointRequestVO {

    private Integer userId;
    private List<OpDeliveryPoint> points;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<OpDeliveryPoint> getPoints() {
        return points;
    }

    public void setPoints(List<OpDeliveryPoint> points) {
        this.points = points;
    }
}
