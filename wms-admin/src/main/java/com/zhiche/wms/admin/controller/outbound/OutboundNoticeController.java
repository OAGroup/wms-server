package com.zhiche.wms.admin.controller.outbound;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.RestfulResponse;
import com.zhiche.wms.dto.base.ResultDTO;
import com.zhiche.wms.dto.outbound.OutNoticeListResultDTO;
import com.zhiche.wms.dto.outbound.OutboundNoticeParamDTO;
import com.zhiche.wms.service.outbound.IOutboundNoticeHeaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/outboundNotice")
public class OutboundNoticeController {

    @Autowired
    private IOutboundNoticeHeaderService noticeHeaderService;

    /**
     * 保存出库通知单
     */
    @PostMapping("/saveOutboundNotice")
    public ResultDTO<Object> saveOutboundNotice(@RequestBody List<OutboundNoticeParamDTO> dtos) {
        ResultDTO<Object> resultDTO = new ResultDTO<>(true, null, "操作成功!");
        Map<String, Object> exMap = noticeHeaderService.saveOutboundNotice(dtos);
        if (!exMap.isEmpty()) {
            resultDTO.setSuccess(false);
            resultDTO.setData(null);
            resultDTO.setMessage(JSONObject.toJSONString(exMap));
        }
        return resultDTO;
    }

    /**
     * 出库通知查询
     */
    @PostMapping("/getOutboundNoticeList")
    public RestfulResponse<Page<OutNoticeListResultDTO>> getOutboundNoticeList(@RequestBody Page<OutNoticeListResultDTO> page) {
        RestfulResponse<Page<OutNoticeListResultDTO>> resultDTO = new RestfulResponse<>(0, "success");
        Page<OutNoticeListResultDTO> resultPage = noticeHeaderService.queryOutboundNotice(page);
        resultDTO.setData(resultPage);
        return resultDTO;
    }

    /**
     * 出库通知导出
     */
    @PostMapping("/exportONData")
    public RestfulResponse<List<OutNoticeListResultDTO>> exportONData(@RequestBody Map<String,String> condition) {
        RestfulResponse<List<OutNoticeListResultDTO>> resultDTO = new RestfulResponse<>(0, "success");
        List<OutNoticeListResultDTO> data = noticeHeaderService.queryExportONData(condition);
        resultDTO.setData(data);
        return resultDTO;
    }

}
