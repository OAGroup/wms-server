package com.zhiche.wms.admin.vo.sys;

import java.util.List;

/**
 * Created by zhaoguixin on 2017/12/14.
 */
public class UserRoleRequestVO {

    private Integer userId;
    private List<Integer> roleIds;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<Integer> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<Integer> roleIds) {
        this.roleIds = roleIds;
    }
}
