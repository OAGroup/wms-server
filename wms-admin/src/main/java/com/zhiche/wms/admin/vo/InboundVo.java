package com.zhiche.wms.admin.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class InboundVo {
    private String key;
    @JsonSerialize(using=ToStringSerializer.class)
    private Long houseId;
    private List<Long> lineIds;
//    private Map<String ,Object> map;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public List<Long> getLineIds() {
        return lineIds;
    }

    public void setLineIds(List<Long> lineIds) {
        this.lineIds = lineIds;
    }

//    public Map<String, Object> getMap() {
//        return map;
//    }
//
//    public void setMap(Map<String, Object> map) {
//        this.map = map;
//    }
}
