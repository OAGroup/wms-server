package com.zhiche.wms.admin.vo.sys;

import java.util.List;

/**
 * Created by zhaoguixin on 2017/12/10.
 */
public class RolePermRequestVO {

    private Integer roleId;
    private List<Integer> permissionIds;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public List<Integer> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Integer> permissionIds) {
        this.permissionIds = permissionIds;
    }
}
