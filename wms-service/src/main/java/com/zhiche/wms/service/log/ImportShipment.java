package com.zhiche.wms.service.log;

import com.zhiche.wms.service.dto.ShipmentDTO;

public interface ImportShipment {

    /**
     * 更新OTM的调度指令至WMS
     * @param shipmentDTO
     */
    void importOTMShipment(ShipmentDTO shipmentDTO);
}
