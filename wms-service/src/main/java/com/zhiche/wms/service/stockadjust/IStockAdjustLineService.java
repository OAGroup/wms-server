package com.zhiche.wms.service.stockadjust;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.stockadjust.StockAdjustLine;

/**
 * <p>
 * 库存调整明细 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
public interface IStockAdjustLineService extends IService<StockAdjustLine> {

}
