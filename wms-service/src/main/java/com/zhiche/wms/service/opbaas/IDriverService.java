package com.zhiche.wms.service.opbaas;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.opbaas.Driver;

/**
 * <p>
 * 司机信息 服务类
 * </p>
 *
 * @author user
 * @since 2018-06-06
 */
public interface IDriverService extends IService<Driver> {

}
