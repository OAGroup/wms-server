package com.zhiche.wms.service.opbaas.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.opbaas.BassUserDeliveryPointMapper;
import com.zhiche.wms.domain.model.opbaas.OpUserDeliveryPoint;
import com.zhiche.wms.service.opbaas.IUserDeliveryPointService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户发车点配置 服务实现类
 * </p>
 *
 * @author user
 * @since 2018-05-24
 */
@Service
public class BassUserDeliveryPointServiceImpl extends ServiceImpl<BassUserDeliveryPointMapper, OpUserDeliveryPoint> implements IUserDeliveryPointService {

}
