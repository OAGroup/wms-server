package com.zhiche.wms.service.dto;

import java.io.Serializable;

/**
 * Created by zhaoguixin on 2018/7/22.
 */
public class OTMEvent implements Serializable {

    /**
     * 导出唯一键(必须)
     */
    private String exportKey;
    /**
     * 回调地址
     */
    private String callBackUrl;
    /**
     * 事件类型(必须)
     * 已入库：BS_WMS_IN
     * 已发运：BS_OP_DELIVERY
     * 已运抵：BS_TRANS_ARRIVED
     * 已寻车：OR_FIND
     * 在途：BS_ENROUTED
     * 绑码：BINDING_CODE
     * 异常：ABNORMAL
     */
    private String eventType;
    /**
     * 事件发生时间(必须)
     */
    private String occurDate;
    /**
     * 事件接收时间(必须)
     */
    private String recdDate;
    /**
     * 发生事件的站点序号
     */
    private int sort;
    /**
     * 状态描述（已入库/已发运/已运抵）(必须)
     */
    private String describe;
    /**
     * 运输订单号(必须)
     */
    private String orderReleaseId;
    /**
     * 指令号(必须)
     */
    private String shipmentId;
    /**
     * 二维码信息
     */
    private String qrCode;
    /**
     * 车架号
     */
    private String vin;

    public String getExportKey() {
        return exportKey;
    }

    public void setExportKey(String exportKey) {
        this.exportKey = exportKey;
    }

    public String getCallBackUrl() {
        return callBackUrl;
    }

    public void setCallBackUrl(String callBackUrl) {
        this.callBackUrl = callBackUrl;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getOccurDate() {
        return occurDate;
    }

    public void setOccurDate(String occurDate) {
        this.occurDate = occurDate;
    }

    public String getRecdDate() {
        return recdDate;
    }

    public void setRecdDate(String recdDate) {
        this.recdDate = recdDate;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getOrderReleaseId() {
        return orderReleaseId;
    }

    public void setOrderReleaseId(String orderReleaseId) {
        this.orderReleaseId = orderReleaseId;
    }

    public String getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(String shipmentId) {
        this.shipmentId = shipmentId;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    @Override
    public String toString() {
        return "OTMEvent{" +
                "exportKey='" + exportKey + '\'' +
                ", callBackUrl='" + callBackUrl + '\'' +
                ", eventType='" + eventType + '\'' +
                ", occurDate='" + occurDate + '\'' +
                ", recdDate='" + recdDate + '\'' +
                ", sort=" + sort +
                ", describe='" + describe + '\'' +
                ", orderReleaseId='" + orderReleaseId + '\'' +
                ", shipmentId='" + shipmentId + '\'' +
                ", qrCode='" + qrCode + '\'' +
                ", vin='" + vin + '\'' +
                '}';
    }
}
