package com.zhiche.wms.service.opbaas.impl;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.opbaas.StatusLogMapper;
import com.zhiche.wms.domain.model.opbaas.StatusLog;
import com.zhiche.wms.service.opbaas.IStatusLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 状态日志 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-08-11
 */
@Service
public class StatusLogServiceImpl extends ServiceImpl<StatusLogMapper, StatusLog> implements IStatusLogService {

}
