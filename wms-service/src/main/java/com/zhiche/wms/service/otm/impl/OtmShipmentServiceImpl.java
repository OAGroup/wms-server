package com.zhiche.wms.service.otm.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.zhiche.wms.core.supports.BaseException;
import com.zhiche.wms.core.supports.enums.TableStatusEnum;
import com.zhiche.wms.core.utils.SnowFlakeId;
import com.zhiche.wms.domain.mapper.opbaas.StatusLogMapper;
import com.zhiche.wms.domain.mapper.otm.OtmOrderReleaseMapper;
import com.zhiche.wms.domain.mapper.otm.ShipmentRecordMapper;
import com.zhiche.wms.domain.model.opbaas.StatusLog;
import com.zhiche.wms.domain.model.otm.OtmOrderRelease;
import com.zhiche.wms.domain.model.otm.OtmShipment;
import com.zhiche.wms.domain.mapper.otm.OtmShipmentMapper;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentForShipDTO;
import com.zhiche.wms.dto.opbaas.resultdto.ShipmentRecordDTO;
import com.zhiche.wms.service.otm.IOtmOrderReleaseService;
import com.zhiche.wms.service.otm.IOtmShipmentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 调度指令 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-13
 */
@Service
public class OtmShipmentServiceImpl extends ServiceImpl<OtmShipmentMapper, OtmShipment> implements IOtmShipmentService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OtmShipmentServiceImpl.class);
    @Autowired
    private SnowFlakeId snowFlakeId;

    @Autowired
    private IOtmOrderReleaseService otmOrderReleaseService;

    @Autowired
    private ShipmentRecordMapper shipmentRecordMapper;
    @Autowired
    private OtmOrderReleaseMapper otmOrderReleaseMapper;

    @Autowired
    private StatusLogMapper statusLogMapper;

    @Override
    @Transactional
    public boolean insertShipment(OtmShipment otmShipment) {
        otmShipment.setId(snowFlakeId.nextId());
        for(OtmOrderRelease otmOrderRelease : otmShipment.getOtmOrderReleaseList()){
            otmOrderRelease.setId(snowFlakeId.nextId());
        }
        return otmOrderReleaseService.insertBatch(otmShipment.getOtmOrderReleaseList()) && insert(otmShipment);
    }

    /**
     * 模糊查询指令列表  -- 关联发车配置点
     *
     */
    @Override
    public List<ShipmentForShipDTO> queryShipmentReleaseList(Page<ShipmentForShipDTO> page, HashMap<String, Object> param, EntityWrapper<ShipmentForShipDTO> ew) {
        return baseMapper.queryShipmentReleaseList(page,param, ew);
    }

    /**
     * 装车发运-点击获取指令详情
     */
    @Override
    public List<ShipmentForShipDTO > getShipDetail(EntityWrapper<ShipmentForShipDTO > ew) {
        return baseMapper.getShipDetail(ew);
    }

    @Override
    public int countShipmentReleaseList(HashMap<String, Object> params, EntityWrapper<ShipmentForShipDTO> ew) {
        return baseMapper.countShipmentReleaseList(params, ew);
    }

    @Override
    public List<ShipmentForShipDTO> queryShipmentReleases(EntityWrapper<ShipmentForShipDTO> dataEW) {
        return baseMapper.queryShipmentReleases(dataEW);
    }

    @Override
    public List<ShipmentRecordDTO> queryShipRecordList (Page<ShipmentRecordDTO> page, EntityWrapper<ShipmentRecordDTO> ew) {
        return shipmentRecordMapper.queryShipRecordList(page,ew);
    }

    @Override
    public List<OtmOrderRelease> queryShipmentStatus (List<Map<String, String>> shipmentGid) {
        if (CollectionUtils.isEmpty(shipmentGid)) {
            throw new BaseException("查询参数为空，请传递指令号和运单号");
        }

        StringBuffer shipmentGids = new StringBuffer();
        StringBuffer releaseGids = new StringBuffer();
        for (Map<String, String> param : shipmentGid) {
            shipmentGids.append(param.get("shipmentGid")).append(",");
            releaseGids.append(param.get("releaseGid")).append(",");
        }

        Wrapper<OtmOrderRelease> ew = new EntityWrapper<>();
        ew.ne("status", TableStatusEnum.STATUS_50.getCode());
        ew.in("shipment_gid", shipmentGids.toString().split(","));
        ew.in("release_gid", releaseGids.toString().split(",")).orderBy("gmt_modified", false);
        List<OtmOrderRelease> otmOrderRelease = otmOrderReleaseMapper.selectList(ew);

        if(CollectionUtils.isNotEmpty(otmOrderRelease)){
            for(OtmOrderRelease orderRelease : otmOrderRelease){
                if(!orderRelease.getStatus().equals(TableStatusEnum.STATUS_BS_DISPATCH.getCode())){
                    Wrapper<StatusLog> statusLogWrapper = new EntityWrapper<>();
                    statusLogWrapper.eq("table_id",orderRelease.getId());
                    statusLogWrapper.eq("table_type",TableStatusEnum.STATUS_10.getCode());
                    statusLogWrapper.eq("status",TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    List<StatusLog> statusLogs = statusLogMapper.selectList(statusLogWrapper);
                    if(CollectionUtils.isNotEmpty(statusLogs)){
                        orderRelease.setStatus(TableStatusEnum.STATUS_BS_DISPATCH.getCode());
                    }
                }
            }
        }
        LOGGER.info("人送/运力根据指令号和运单号查询结果为{}", otmOrderRelease);
        return otmOrderRelease;
    }
}
