package com.zhiche.wms.service.constant;

/**
 * Created by zhaoguixin on 2018/6/19.
 */
public class OtmShipmentType {

    public static final String FRONT_MOVE ="前端移库段";
    public static final String BEHIND_MOVE ="末端移库段";
    public static final String TRUNK_LINE ="干线段";
    public static final String PICK_UP ="提车段";
    public static final String DISPATCH ="配送段";
    public static final String FRONT_TRANSFER ="前端中转段";
    public static final String BEHIND_TRANSFER ="末端中转段";

}
