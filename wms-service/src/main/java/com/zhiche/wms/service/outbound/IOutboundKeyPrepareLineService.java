package com.zhiche.wms.service.outbound;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.outbound.OutboundKeyPrepareLine;

import java.util.Map;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 16:46 2018/12/24
 */
public interface IOutboundKeyPrepareLineService  extends IService<OutboundKeyPrepareLine> {
    /**
     * 修改钥匙状态
     * @param param
     */
    void updateKeyPrepareStatus (Map<String, String> param);
}
