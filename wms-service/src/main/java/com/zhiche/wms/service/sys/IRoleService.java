package com.zhiche.wms.service.sys;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.sys.Permission;
import com.zhiche.wms.domain.model.sys.Role;

import java.util.List;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
public interface IRoleService extends IService<Role> {

    /**
     * 删除角色同时删除角色权限
     *
     * @param roleIds 角色ID数组
     */
    boolean deleteRole(List<Integer> roleIds) throws Exception;

    /**
     * 给角色分配权限
     *
     * @param roleId        角色ID
     * @param permissionIds 所指派的权限
     */
    Integer assignPermission(Integer roleId, List<Integer> permissionIds) throws Exception;

    /**
     * 得到角色所拥有的全年
     */
    List<Permission> listOwnPermission(Integer roleId);

    /**
     * 得到角色所拥有的全部权限
     */
    List<Permission> listOwnAllPermission(Integer roleId);

    /**
     * 分页查询角色
     */
    Page<Role> queryRolePage(Page<Role> page);

    /**
     * 得到角色所拥有的全部最底层权限
     */
    List<Permission> getOwnBottomPermission(Integer roleId);
}
