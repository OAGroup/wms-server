package com.zhiche.wms.service.sys.impl;

import com.zhiche.wms.domain.model.sys.Permission;
import com.zhiche.wms.domain.mapper.sys.PermissionMapper;
import com.zhiche.wms.service.sys.IPermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 权限 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-06-19
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

    @Override
    public List<Permission> listAppPermission(Integer userId){
        List<Permission> permissionList = baseMapper.selectPermListByUserId(userId,"APP");
        return permissionList;
    }

    @Override
    public List<Permission> listFistPermission(Integer userId) {

        List<Permission> permissionList = baseMapper.selectPermListByUserId(userId,"PC");

        List<Permission> permissions = new ArrayList<>();
        for(Permission permission :permissionList){
            if(permission.getPid().equals(0)){
                permissions.add(permission);
            }
        }
        return permissions;
    }

    @Override
    public List<Permission> listChildPermission(Integer userId, Integer pid) {
        List<Permission> permissionList = this.baseMapper.selectPermListByUserId(userId,"PC");
        List<Permission> result = new ArrayList<>();
        setSubMenu(pid,result,permissionList);
        return result;
    }

    private void setSubMenu(Integer pid, List<Permission> result, List<Permission> source) {
        for (Permission perm : source) {
            if (perm.getPid().equals(pid)) {
                result.add(perm);
                setSubMenu(perm.getId(), result, source);
            }
        }
    }
}
