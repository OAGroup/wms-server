package com.zhiche.wms.service.stock.impl;


import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.zhiche.wms.domain.mapper.stock.StockDetailMapper;
import com.zhiche.wms.domain.model.stock.StockDetail;
import com.zhiche.wms.service.stock.IStockDetailService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库存明细账 服务实现类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-05-30
 */
@Service
public class StockDetailServiceImpl extends ServiceImpl<StockDetailMapper, StockDetail> implements IStockDetailService {

}
