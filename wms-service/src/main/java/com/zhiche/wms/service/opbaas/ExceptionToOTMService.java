package com.zhiche.wms.service.opbaas;

import com.zhiche.wms.dto.opbaas.paramdto.CommonConditionParamDTO;

/**
 * <p>
 * 异常推送otm
 * </p>
 *
 * @author yzy
 * @since 2018-11-09
 */
public interface ExceptionToOTMService {

    void isCanSend(CommonConditionParamDTO conditionParamDTO);

    String isSend(String vin ,String originName);
}
