package com.zhiche.wms.service.dto;

import java.io.Serializable;

public class ShipParamDTO extends OTMEvent implements Serializable {

    private String plateNo;//车牌号
    private String trailerNo;//挂车号
    private String vehicleName;//标准车型
    private String originProvince;
    private String originCity;
    private String originCounty;
    private String originAddr;
    private String destProvince;
    private String destCity;
    private String destCounty;
    private String destAddr;
    private String driverGid;
    private String providerGid;
    private String shipTime;
    private String originCode;
    private String destCode;
    private String cusOrderNo;
    private String cusWaybill;

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public void setDestCode(String destCode) {
        this.destCode = destCode;
    }

    public String getCusOrderNo() {
        return cusOrderNo;
    }

    public void setCusOrderNo(String cusOrderNo) {
        this.cusOrderNo = cusOrderNo;
    }

    public String getCusWaybill() {
        return cusWaybill;
    }

    public void setCusWaybill(String cusWaybill) {
        this.cusWaybill = cusWaybill;
    }

    public String getPlateNo() {
        return plateNo;
    }

    public void setPlateNo(String plateNo) {
        this.plateNo = plateNo;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getOriginProvince() {
        return originProvince;
    }

    public void setOriginProvince(String originProvince) {
        this.originProvince = originProvince;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getOriginCounty() {
        return originCounty;
    }

    public void setOriginCounty(String originCounty) {
        this.originCounty = originCounty;
    }

    public String getOriginAddr() {
        return originAddr;
    }

    public void setOriginAddr(String originAddr) {
        this.originAddr = originAddr;
    }

    public String getDestProvince() {
        return destProvince;
    }

    public void setDestProvince(String destProvince) {
        this.destProvince = destProvince;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public String getDestCounty() {
        return destCounty;
    }

    public void setDestCounty(String destCounty) {
        this.destCounty = destCounty;
    }

    public String getDestAddr() {
        return destAddr;
    }

    public void setDestAddr(String destAddr) {
        this.destAddr = destAddr;
    }

    public String getDriverGid() {
        return driverGid;
    }

    public void setDriverGid(String driverGid) {
        this.driverGid = driverGid;
    }

    public String getProviderGid() {
        return providerGid;
    }

    public void setProviderGid(String providerGid) {
        this.providerGid = providerGid;
    }

    public String getShipTime() {
        return shipTime;
    }

    public void setShipTime(String shipTime) {
        this.shipTime = shipTime;
    }

    @Override
    public String toString() {
        return "ShipParamDTO{" +
                "plateNo='" + plateNo + '\'' +
                ", trailerNo='" + trailerNo + '\'' +
                ", vehicleName='" + vehicleName + '\'' +
                ", originProvince='" + originProvince + '\'' +
                ", originCity='" + originCity + '\'' +
                ", originCounty='" + originCounty + '\'' +
                ", originAddr='" + originAddr + '\'' +
                ", destProvince='" + destProvince + '\'' +
                ", destCity='" + destCity + '\'' +
                ", destCounty='" + destCounty + '\'' +
                ", destAddr='" + destAddr + '\'' +
                ", driverGid='" + driverGid + '\'' +
                ", providerGid='" + providerGid + '\'' +
                ", shipTime='" + shipTime + '\'' +
                '}';
    }
}
