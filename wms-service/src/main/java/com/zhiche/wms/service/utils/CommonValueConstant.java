package com.zhiche.wms.service.utils;

/**
 * @Author: caiHua
 * @Description:
 * @Date: Create in 17:17 2018/12/12
 */
public class CommonValueConstant {
    /**
     * 自动入库状态
     */
    public static final String INBOUND_NOTICE_LINE_10 = "10";

    /** 自动出库状态 10:未出库*/
    public static final String OUTBOUND_NOTICE_LINE_10 = "10";

    /**自动出库状态 20:部分出库*/
    public static final String OUTBOUND_NOTICE_LINE_20 = "20";


    /**
     * sys_config 无人值守自动入库code：NOPERSON_INBOUND
     */
    public static final String NOPERSON_INBOUND = "NOPERSON_INBOUND";

    /**
     * sys_config 无人值守自动出库code：NOPERSON_OUTBOUND
     */
    public static final String NOPERSON_OUTBOUND = "NOPERSON_OUTBOUND";
    /**
     * sys_config 无人值守自动入库flag :inBound
     */
    public static final String INBOUND = "inBound";
    /**
     * sys_config 无人值守自动出库code：outBound
     */
    public static final String OUTBOUND = "outBound";


    /**
     * 仓库查询类型 config_type: WAREHOUSE
     */
    public static final String CONFIG_TYPE_WAREHOUSE = "WAREHOUSE";

    /**
     * 配置是否勾选 1 已选择配置,0 未配置
     */
    public static final String CONFIGED_1 = "1";

    /**
     * 实车校验状态: 1校验通过
     */
    public static final String VEHICLE_CHECKOUT_STATUS_1 = "1";

    /** 实车校验状态: 2校验不通过*/
    public static final String VEHICLE_CHECKOUT_STATUS_2 = "2";

    /**w_outbound_prepare_line状态(10:未开始,20:已开始,30:已完成)*/
    public static final String OUTBOUND_STATUS_20 = "20";

    /**w_outbound_prepare_line状态(10:未开始,20:已开始,30:已完成)*/
    public static final String OUTBOUND_STATUS_10 = "10";


    /**w_outbound_prepare_header 状态(10:备料计划,20:开始执行,30:部分备料,40:全部完成)*/
    public static final String OUTBOUND_HEADER_STATUS_30 = "30";

    /**cancel_store_log 退库领取状态（10：未领取，20：领取；30：完成）*/
    public static final String RECEIVE_STATUS_20 = "20";

    public static final String RECEIVE_STATUS_10 = "10";

}
