package com.zhiche.wms.service.sys;

import com.baomidou.mybatisplus.service.IService;
import com.zhiche.wms.domain.model.sys.AppVersion;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zhaoguixin
 * @since 2018-07-17
 */
public interface IAppVersionService extends IService<AppVersion> {

    /**
     * 获取安卓版本更新
     */
    AppVersion getLastVersionAndroid();

    /**
     * 获取ios版本更新
     */
    AppVersion getLastVersionIos();

}
