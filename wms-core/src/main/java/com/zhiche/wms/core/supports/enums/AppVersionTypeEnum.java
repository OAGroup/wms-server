package com.zhiche.wms.core.supports.enums;

public enum AppVersionTypeEnum {
    ANDROID("android", "系统异常"),
    IOS("ios", "系统异常");

    private final String code;
    private final String detail;

    public String getCode() {
        return code;
    }

    public String getDetail() {
        return detail;
    }

    AppVersionTypeEnum(String code, String detail) {
        this.code = code;
        this.detail = detail;
    }


}
